/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entities.Produto;
import java.io.IOException;
import Model.produtoModel;
import java.util.Random;

/**
 *
 * @author andre
 */
public class produtoController {
    public void cria(String nome, float preco, int quantidade) throws IOException{
       Random gerador = new Random();
       Produto produto = new Produto(gerador.nextInt(10000000), nome, preco, quantidade);
       grava(produto);
   }
    
    public void grava(Produto produto) throws IOException{
       produtoModel model = new produtoModel();
       model.grava(produto);
   }
   public void deleta(int numero) throws IOException{
       produtoModel model = new produtoModel();
       model.deleta(numero);
   }
   public void visualiza() throws IOException{
       produtoModel model = new produtoModel();
       model.visualiza();
   }
}

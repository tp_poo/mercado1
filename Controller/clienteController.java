/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entities.Cliente;
import Model.clienteModel;
import java.io.IOException;
import java.util.Random;
import Entities.Cliente;
/**
 *
 * @author andre
 */
public class clienteController {

    public void cria(String nome, double telefone, String endereco) throws IOException {
        Random gerador = new Random();
        Cliente cliente = new Cliente(gerador.nextInt(10000000), nome, telefone, endereco);
        grava(cliente);
    }

    public void grava(Cliente cliente) throws IOException {
        clienteModel model = new clienteModel();
        model.grava(cliente);
    }

    public void deleta(int numero) throws IOException {
        clienteModel model = new clienteModel();
        model.deleta(numero);
    }

    public void visualiza() throws IOException {
        clienteModel model = new clienteModel();
        model.visualiza();
    }

    public String buscaItem(int id) throws IOException {
        clienteModel model = new clienteModel();
        String bruta = model.buscaItem(id);
        String nome = nomeCliente(bruta);
        return nome;
    }

    public String nomeCliente(String bruta) {
        String nome, endereco;
        double telefone;
        int num, num2, num3, num4, id = 0;
        while (true) {
            num = bruta.indexOf("*");
            id = Integer.parseInt(bruta.substring(0, num));

            num2 = bruta.indexOf("*", num + 1);
            nome = bruta.substring(num + 1, num2);

            num3 = bruta.indexOf("*", num2 + 1);
            endereco = bruta.substring(num2 + 1, num3);

            num4 = bruta.indexOf("*", num3 + 1);
            telefone = Double.parseDouble(bruta.substring(num3 + 1, num4));
            break;
        }
        Cliente cliente = new Cliente(id,nome,telefone, endereco);
        
        return nome;
    }
    
    public Cliente obtemCliente(int numero) throws IOException {
        clienteModel model = new clienteModel();
        String bruta = model.buscaItem(numero);
        String nome, endereco;
        double telefone;
        int num, num2, num3, num4, id = 0;
        while (true) {
            num = bruta.indexOf("*");
            id = Integer.parseInt(bruta.substring(0, num));

            num2 = bruta.indexOf("*", num + 1);
            nome = bruta.substring(num + 1, num2);

            num3 = bruta.indexOf("*", num2 + 1);
            endereco = bruta.substring(num2 + 1, num3);

            num4 = bruta.indexOf("*", num3 + 1);
            telefone = Double.parseDouble(bruta.substring(num3 + 1, num4));
            break;
        }
        Cliente cliente = new Cliente(id,nome,telefone, endereco);
        
        return cliente;
    }
}

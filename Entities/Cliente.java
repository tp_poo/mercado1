/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author andre
 */
public class Cliente extends Entidade{
   private String nome;
   private double telefone;
   private String endereco;

    /**
     *
     */

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return "Cliente{" + "nome=" + nome + ", telefone=" + telefone + ", endereco=" + endereco + '}';
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getTelefone() {
        return telefone;
    }

    public void setTelefone(double telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Cliente(int numero, String nome, double telefone, String endereco) {
        super(numero);
        this.nome = nome;
        this.telefone = telefone;
        this.endereco = endereco;
    }
}

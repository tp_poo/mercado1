/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Entities.Cliente;
import View.cliente.listar;
import View.cliente.listarCliente;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import Model.Singleton;
/**
 *
 * @author andre
 */
public class clienteModel {
    public clienteModel(){
    }
    public void grava(Cliente cliente) throws IOException{
        Singleton instance = Singleton.getInstance();
        instance.grava(cliente.getNumero() + "*" + cliente.getNome() + "*" + cliente.getEndereco() + "*" + cliente.getTelefone() + "*" + "\n","cliente.txt");
    }
    
//    public void grava(Cliente cliente) throws FileNotFoundException, IOException{
//        try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("cliente.txt", true))){
//            buffWrite.append(cliente.getNumero() + "*" + cliente.getNome() + "*" + cliente.getEndereco() + "*" + cliente.getTelefone() + "*" + "\n");
//            buffWrite.close();
//        }
//    }
    
    public void deleta(int numero) throws FileNotFoundException, IOException{
        String avalia = "";
        int contador = 0, num = 0;
        try(BufferedReader buffRead = new BufferedReader(new FileReader("cliente.txt"))){
            while (true) {
                avalia = buffRead.readLine();
                if(avalia == null)break;
                num = avalia.indexOf("*");
                if(Integer.parseInt(avalia.substring(0, num)) != numero){
//                    System.out.println(avalia);
//                    break;
                    try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("cliente.txt"))){
                        buffWrite.append(avalia + "\n");
                        buffWrite.close();
                        contador++;
                    }
                }
//                System.out.println(avalia.substring(0,1));

            }
            if(contador == 0){
                try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("cliente.txt"))){
                    buffWrite.append("");
                    buffWrite.close();
                }
            }
            buffRead.close();
        }
    }
    
    public void visualiza() throws IOException{
//        listar view = new listar();
        listarCliente l = new listarCliente();
        l.setVisible(true);
        String leitura = "";
        String leituraConcat = "";
        try(BufferedReader buffRead = new BufferedReader(new FileReader("cliente.txt"))){
            while (true) {
                if(leitura == null)break;
                leitura = buffRead.readLine();
//                System.out.println(leitura);
                if(leitura != null){
//                    leituraConcat = leitura.concat("\n");
                    leituraConcat = leituraConcat.concat(leitura);
                    leituraConcat = leituraConcat.concat("\n");
//                    System.out.println(leitura);
                }
//                    view.visualiza(leitura);
//                break;
            }
//            System.out.println(leituraConcat);exit(1);
            l.list(leituraConcat);
        }
    }
    
    public String buscaItem(int id) throws FileNotFoundException, IOException{
        String leitura = "";
        int num = 0, idBusca = 0;
        try(BufferedReader buffRead = new BufferedReader(new FileReader("cliente.txt"))){
            while (true) {
                leitura = buffRead.readLine();
                if(leitura == null)break;
                num = leitura.indexOf("*");
                idBusca = Integer.parseInt(leitura.substring(0, num));
                if(idBusca == id){
                    return leitura;
                }
            }
        }
        return null;
    }
}

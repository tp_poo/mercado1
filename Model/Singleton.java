/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Entities.Cliente;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author andre
 */
public class Singleton {
    private static Singleton uniqueInstance;
 
    private Singleton() {
    }
 
    public static synchronized Singleton getInstance() { //certeza que o método nunca será acessado por duas threads ao mesmo tempo
        if (uniqueInstance == null)
            uniqueInstance = new Singleton();
 
        return uniqueInstance;
    }
    
    public void grava(String palavra, String caminho) throws FileNotFoundException, IOException{
        try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter(caminho, true))){
            buffWrite.append(palavra);
            buffWrite.close();
        }
    }
}

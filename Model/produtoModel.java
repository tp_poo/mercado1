/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Entities.Produto;
import View.produto.listar;
import View.produto.listarProduto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
/**
 *
 * @author andre
 */
public class produtoModel {
    public produtoModel(){
    }
    
    public void grava(Produto produto) throws IOException{
        Singleton instance = Singleton.getInstance();
        instance.grava(produto.getNumero() + "*" + produto.getNome() + "*" + produto.getPreco() + "*" + produto.getQuantidade() + "*" + "\n", "produto.txt");
    }
    
//    public void grava(Produto produto) throws FileNotFoundException, IOException{
//        try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("produto.txt", true))){
//            buffWrite.append(produto.getNumero() + "*" + produto.getNome() + "*" + produto.getPreco() + "*" + produto.getQuantidade() + "*" + "\n");
//            buffWrite.close();
//        }
//    }
    
    public void deleta(int numero) throws FileNotFoundException, IOException{
        String avalia = "";
        int contador = 0, num = 0;
        try(BufferedReader buffRead = new BufferedReader(new FileReader("produto.txt"))){
            while (true) {
                avalia = buffRead.readLine();
                if(avalia == null)break;
                num = avalia.indexOf("*");
                if(Integer.parseInt(avalia.substring(0, num)) != numero){
//                    System.out.println(avalia);
//                    break;
                    try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("produto.txt"))){
                        buffWrite.append(avalia + "\n");
                        buffWrite.close();
                        contador++;
                    }
                }

            }
            if(contador == 0){
                try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("produto.txt"))){
                    buffWrite.append("");
                    buffWrite.close();
                }
            }
            buffRead.close();
        }
    }
    
    public void visualiza() throws IOException{
//        listar view = new listar();
        listarProduto l = new listarProduto();
        l.setVisible(true);
        String leitura = "";
        String leituraConcat = "";
        try(BufferedReader buffRead = new BufferedReader(new FileReader("produto.txt"))){
            while (true) {
                if(leitura == null)break;
                leitura = buffRead.readLine();
//                System.out.println(leitura);
                if(leitura != null){
//                    view.visualiza(leitura);
                   leituraConcat = leituraConcat.concat(leitura);
                   leituraConcat = leituraConcat.concat("\n"); 
                }
            }
            l.list(leituraConcat);
        }        
    }
    public String buscaItem(int id) throws FileNotFoundException, IOException{
        String leitura = "";
        int num = 0, idBusca = 0;
        try(BufferedReader buffRead = new BufferedReader(new FileReader("produto.txt"))){
            while (true) {
                leitura = buffRead.readLine();
                if(leitura == null)break;
                num = leitura.indexOf("*");
                idBusca = Integer.parseInt(leitura.substring(0, num));
                if(idBusca == id){
//                    System.out.println(leitura);
                    return leitura;
                }
            }
        }
        return null;
    }
}

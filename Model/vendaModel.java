/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Entities.Venda;
import View.venda.listar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
/**
 *
 * @author andre
 */
public class vendaModel {
    public vendaModel(){
    }
    public void grava(Venda venda) throws IOException{
        Singleton instance = Singleton.getInstance();
        instance.grava(venda.getNumero() + "*" + venda.getCliente() + "*" + venda.getSaida() + "*" + venda.getValor() + "*" + venda.getDatareg() + "*" + "\n", "venda.txt");
    }
    
//    public void grava(Venda venda) throws FileNotFoundException, IOException{
//        try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("venda.txt", true))){
//            buffWrite.append(venda.getNumero() + "*" + venda.getCliente() + "*" + venda.getSaida() + "*" + venda.getValor() + "*" + venda.getDatareg() + "*" + "\n");
//            buffWrite.close();
//        }
//    }
    
    public void deleta(int numero) throws FileNotFoundException, IOException{
        String avalia = "";
        int contador = 0, num = 0;
        try(BufferedReader buffRead = new BufferedReader(new FileReader("venda.txt"))){
            while (true) {
                avalia = buffRead.readLine();
                if(avalia == null)break;
                num = avalia.indexOf("*");
                if(Integer.parseInt(avalia.substring(0, num)) != numero){
//                    System.out.println(avalia);
//                    break;
                    try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("venda.txt"))){
                        buffWrite.append(avalia + "\n");
                        buffWrite.close();
                        contador++;
                    }
                }
//                System.out.println(avalia.substring(0,1));

            }
            if(contador == 0){
                try(BufferedWriter buffWrite = new BufferedWriter(new FileWriter("venda.txt"))){
                    buffWrite.append("");
                    buffWrite.close();
                }
            }
            buffRead.close();
        }
    }
    
    public void visualiza() throws IOException{
        listar view = new listar();
        String leitura = "";
        
        try(BufferedReader buffRead = new BufferedReader(new FileReader("venda.txt"))){
            while (true) {
                if(leitura == null)break;
                leitura = buffRead.readLine();
//                System.out.println(leitura);
                if(leitura != null)view.visualiza(leitura);
//                break;
            }
        }
        
        
    }
}
